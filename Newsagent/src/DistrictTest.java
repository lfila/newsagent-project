import junit.framework.TestCase;

public class DistrictTest extends TestCase {
	
//Equivalence Partitioning tests
	
	//Test #: 1
	//Objective: invalid partition
	//Input(s): dis_description = "h"
	//Expected O/P: false
	public void testcreateDistrict001() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("h");
			assertEquals(false, result);
			
			//fail("Fail: One of 4 values is invalid");
		}
		catch(DistrictExceptionHandler e)
		{
			assertEquals("assertEquals: One value invalid", e.getMessage());
		}
	}
	
	//Test #: 2
	//Objective: valid partition
	//Input(s): dis_description = "aaa"
	//Expected O/P: true
	public void testcreateDistrict002() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("hhh");
			assertEquals(true, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 3
	//Objective: invalid partition
	//Input(s): "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" (100)
	//Expected O/P: false
	public void testcreateDistrict003() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij");
			assertEquals(false, result);
			//fail("Fail: One of value is invalid");
		}
		catch(DistrictExceptionHandler e)
		{
			//assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
//Boundary Value Analysis
	
	//Test #: 4
	//Objective: invalid partition low boundary values
	//Input(s): ""
	//Expected O/P: false
	public void testcreateDistrict004() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("");
			assertEquals(false, result);
			//fail("Fail: One value is invalid");
		}
		catch(DistrictExceptionHandler e)
		{

			assertEquals("assertEquals: One  value invalid", e.getMessage());
		}
	}
	
	//Test #: 5
	//Objective: invalid partition high boundary values
	//Input(s): "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
	//Expected O/P: false
	public void testcreateDistrict005() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij");
			assertEquals(false, result);
			//fail("Fail: One value is invalid");
		}
		catch(DistrictExceptionHandler e)
		{
			assertEquals("assertEquals: One value invalid", e.getMessage());
		}
	}
	
	//Test #: 6
	//Objective: valid partition low boundary values
	//Input(s): "hh"
	//Expected O/P: true
	public void testcreateDistrict006() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("hh");
			assertEquals(true, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 7
	//Objective: valid partition high boundary values
	//Input(s): abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaa (99)
	//Expected O/P: true
	public void testcreateDistrict007() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaa");
			assertEquals(true, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 8
	//Objective: invalid partition low boundary values
	//Input(s): abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaaaa
	//Expected O/P: false
	public void testcreateDistrict008() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaaaa");
			assertEquals(false, result);
			//fail("Fail: One value is invalid");
		}
		catch(DistrictExceptionHandler e)
		{
			assertEquals("assertEquals: One value invalid", e.getMessage());
		}
	}
	
	//Test #: 9
	//Objective: invalid partition high boundary values
	//Input(s): abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	//Expected O/P: false
	public void testcreateDistrict009() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.createDistrict("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			assertEquals(false, result);
			//fail("Fail: One value is invalid");
		}
		catch(DistrictExceptionHandler e)
		{
			assertEquals("assertEquals: One value invalid", e.getMessage());
		}
	}


	//Test #: 11
	//Objective: deleteDistrict database interaction succeeds (tested through StartUp deleteDis)
	//Input(s): 3
	//Expected O/P: true (4 exists in example)
	public void testdeleteDistrict011() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.deleteDistrict(4);
			
			assertEquals(true, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 12
	//Objective: deleteDistrict database interaction fails (tested through StartUp deleteDis)
	//Input(s): 20
	//Expected O/P: false (20 doesn't exist in example)
	public void testdeleteDistrict012() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.deleteDistrict(20);
			
			assertEquals(false, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 13
	//Objective: deleteDistrct database interaction fails (tested through StartUp deleteDis)
	//Input(s): 2
	//Expected O/P: false (2 exists in example but can't be deleted as other table (delivery) depends on it)
	public void testdeleteDistrict013() {
		District testObj = new District();
		
		try
		{
			boolean result = testObj.deleteDistrict(20);
			
			assertEquals(false, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 10
	//Objective: insertDistrict database interaction succeeds (tested through StartUp createDis)
	// insertDistrict (District) tested with valid district only as it's
	// impossible for insertDistrict to be called without a valid District
	// 
	//Input(s): test.
	//Expected O/P: true
	/*
	public void testcreateDis010() {
		District testObj = new District();
		
		try
		{
			District validDistrict = new District();
			boolean result = testObj.insertDistrict(validDistrict);
			
			assertEquals(true, result);
		}
		catch(DistrictExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
}
	*/

