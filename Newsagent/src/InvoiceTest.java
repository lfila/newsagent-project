import junit.framework.TestCase;

public class InvoiceTest extends TestCase {
	
//Equivalence Partitioning tests
	
	//Test #: 1
	//Objective: invalid partition
	//Input(s): inv_date = "01/01/20", due_date = "07/01/20", total = -25.00, is_paid = true, cus_id = -4
	//Expected O/P: false
	public void testcreateInvoice001() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/20", "07/01/20", -25.00, true, -4);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
	//Test #: 2
	//Objective: valid partition
	//Input(s): inv_date = "01/01/2020", due_date = "07/01/2020", total = 42.90, is_paid = true, cus_id = 2
	//Expected O/P: true
	public void testcreateInvoice002() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/2020", "07/01/2020", 42.90, true, 2);
			assertEquals(true, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 3
	//Objective: invalid partition
	//Input(s): inv_date = "001/001/02020", due_date = "007/001/02020", total = 100100.00, is_paid = true, cus_id = 0
	//Expected O/P: false
	public void testcreateInvoice003() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("001/001/02020", "007/001/02020", 100100.00, true, 0);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
//Boundary Value Analysis
	
	//Test #: 4
	//Objective: invalid partition low boundary values
	//Input(s): inv_date = "", due_date = "", total = Double.MIN_VALUE, is_paid = true, cus_id = Integer.MIN_VALUE
	//Expected O/P: false
	public void testcreateInvoice004() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("", "", Double.MIN_VALUE, true, Integer.MIN_VALUE);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
	//Test #: 5
	//Objective: invalid partition high boundary values
	//Input(s): inv_date = "01/01/202", due_date = "07/01/202", total = -0.01, is_paid = true, cus_id = 0
	//Expected O/P: false
	public void testcreateInvoice005() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/202", "07/01/202", -0.01, true, 0);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
	//Test #: 6
	//Objective: valid partition low boundary values
	//Input(s): inv_date = "01/01/2020", due_date = "07/01/2020", total = 0.00, is_paid = true, cus_id = 1
	//Expected O/P: true
	public void testcreateInvoice006() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/2020", "07/01/2020", 0.00, true, 1);
			assertEquals(true, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 7
	//Objective: valid partition high boundary values
	//Input(s): inv_date = "01/01/2020", due_date = "07/01/2020", total = 99999.99, is_paid = true, cus_id = Integer.MAX_VALUE -1
	//Expected O/P: true
	public void testcreateInvoice007() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/2020", "07/01/2020", 99999.99, true, Integer.MAX_VALUE - 1);
			assertEquals(true, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 8
	//Objective: invalid partition low boundary values
	//Input(s): inv_date = "01/01/20200", due_date = "07/01/20200", total = 100000.00, is_paid = true, cus_id = Integer.MAX_VALUE
	//Expected O/P: false
	public void testcreateInvoice008() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/20200", "07/01/20200", 100000.00, true, Integer.MAX_VALUE);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
	//Test #: 9
	//Objective: invalid partition high boundary values
	//Input(s): inv_date = "01/01/202000000", due_date = "07/01/202000000", total = Double.MAX_VALUE, is_paid = true, cus_id = Integer.MAX_VALUE
	//Expected O/P: false
	public void testcreateInvoice009() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.createInvoice("01/01/202000000", "07/01/202000000", -25.00, true, Integer.MAX_VALUE);
			assertEquals(false, result);
			//fail("Fail: One of 4 values is invalid");
		}
		catch(InvoiceExceptionHandler e)
		{
			assertEquals("assertEquals: One of 4 values invalid", e.getMessage());
		}
	}
	
	//Test #: 10
	//Objective: insertInvoice database interaction succeeds (tested through StartUp createInv)
	// insertInvoice (Invoice) tested with valid invoice only as it's
	// impossible for insertInvoice to be called without a valid Invoice
	//Input(s): insertInvoice(new Invoice("01/01/2020", "07/01/2020", 42.90, true, 2));
	//Expected O/P: true
	public void testinsertInvoice010() {
		Invoice testObj = new Invoice();
		
		try
		{
			Invoice validInvoice = new Invoice("01/01/2020", "07/01/2020", 42.90, true, 2);
			boolean result = testObj.insertInvoice(validInvoice);
			
			assertEquals(true, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 11
	//Objective: deleteInvoice database interaction succeeds (tested through StartUp deleteInv)
	//Input(s): 3
	//Expected O/P: true (4 exists in example)
	public void testdeleteInvoice011() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.deleteInvoice(4);
			
			assertEquals(true, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 12
	//Objective: deleteInvoice database interaction fails (tested through StartUp deleteInv)
	//Input(s): 20
	//Expected O/P: false (20 doesn't exist in example)
	public void testdeleteInvoice012() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.deleteInvoice(20);
			
			assertEquals(false, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
	
	//Test #: 13
	//Objective: deleteInvoice database interaction fails (tested through StartUp deleteInv)
	//Input(s): 2
	//Expected O/P: false (2 exists in example but can't be deleted as other table (delivery) depends on it)
	public void testdeleteInvoice013() {
		Invoice testObj = new Invoice();
		
		try
		{
			boolean result = testObj.deleteInvoice(20);
			
			assertEquals(false, result);
		}
		catch(InvoiceExceptionHandler e)
		{
			fail("Fail: No Exception Expected");
		}
	}
}