public class DistrictExceptionHandler extends Exception 
{

    String message;
    
    public DistrictExceptionHandler(String errMessage)
    {
        message = errMessage;
    }
    
    public String getMessage() 
    {
        return message;
    }
}