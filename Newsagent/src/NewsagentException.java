
public class NewsagentException extends Exception 
{
	String message;
	
	public NewsagentException(String errMessage)
	{
		message = errMessage;
	}
	
	public String getMessage() 
	{
		return message;
	}
}
