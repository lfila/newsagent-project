import junit.framework.TestCase;

public class CustomerTest extends TestCase 
{
	
	//createCustomer JUnits 
	
	//EP
	
	//Test #: 1
	//Objective: Invalid values
	//Input(s):	"e", "", 4, 0
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer001()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("e", "", 4, 0);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//Test #: 2
	//Objective: Invalid values
	//Input(s): "isrZ0RRzYjCTXQfFbyDs0GG1YydEdcBDZ5invIJmkDEa7VomlxzQET", "Ahi45oI3zqLffhC6uW3xY29zJSEx4t7RdLENai2RRol4px2sT0Bhn7Jz18lRDiZQ1WlFdljy9YeL3tbVEO6teKHGPnuQ112nzzgLyC", 102, 44
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer002()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("isrZ0RRzYjCTXQfFbyDs0GG1YydEdcBDZ5invIJmkDEa7VomlxzQET", "Ahi45oI3zqLffhC6uW3xY29zJSEx4t7RdLENai2RRol4px2sT0Bhn7Jz18lRDiZQ1WlFdljy9YeL3tbVEO6teKHGPnuQ112nzzgLyC", 102, 44);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//Test #: 3
	//Objective: Valid values
	//Input(s): "david", "athlone, westmeath", 23, 6
	//Expected Output(s): true
	
	public void testcreateCustomer003()
	{
		Customer testObj = new Customer();
		
		try
		{
			assertEquals(true, testObj.createCustomer("david", "athlone, westmeath", 22, 6)); 
		}
		catch(NewsagentException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 4
	//Objective: Invalid values
	//Input(s): "!", "@", 0, 0
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer004()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("!", "@", 0, 0);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//BVA
	
	//Test #: 5
	//Objective: Invalid values
	//Input(s): "", "", 0, 0
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer005()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("", "", 0, 0);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//Test #: 6
	//Objective: Invalid values
	//Input(s): "r", "e", 17, 0
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer006()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("r", "e", 17, 0);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//Test #: 7
	//Objective: Valid values
	//Input(s): "er", "tt", 18, 1
	//Expected Output(s): true
	
	public void testcreateCustomer007()
	{
		Customer testObj = new Customer();
		
		try
		{
			assertEquals(true, testObj.createCustomer("er", "tt", 18, 1)); 
		}
		catch(NewsagentException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 8
	//Objective: Valid values
	//Input(s): "TCvJbeJZxSRBm2i4Emiib82BKUaaDGwZuGqrvuPFJ27JIz92Qn", "KnYwAfhAROkyob2AKid3zbo7sEd45cwQGAgNUK1nxS4n2AQqlUZrC3k1iYBLwxWORCw9CLjFxRyolnHM2cLnr12n444FypngQMsI", 100, 10000
	//Expected Output(s): true
	
	public void testcreateCustomer008()
	{
		Customer testObj = new Customer();
		
		try
		{
			assertEquals(true, testObj.createCustomer("TCvJbeJZxSRBm2i4Emiib82BKUaaDGwZuGqrvuPFJ27JIz92Qn", "KnYwAfhAROkyob2AKid3zbo7sEd45cwQGAgNUK1nxS4n2AQqlUZrC3k1iYBLwxWORCw9CLjFxRyolnHM2cLnr12n444FypngQMsI", 100, 10000)); 
		}
		catch(NewsagentException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 9
	//Objective: Invalid values
	//Input(s): "TCvJbeJZxSRBm2i4Emiib82BKUaaDGwZuGqrvuPFJ27JIz92Qne", "KnYwAfhAROkyob2AKid3zbo7sEd45cwQGAgNUK1nxS4n2AQqlUZrC3k1iYBLwxWORCw9CLjFxRyolnHM2cLnr12n444FypngQMsIe", 101, 10001
	//Expected Output(s): Exception thrown with "One of three values invalid" message
	
	public void testcreateCustomer009()
	{
		Customer testObj = new Customer();
		
		try
		{
			boolean result = testObj.createCustomer("TCvJbeJZxSRBm2i4Emiib82BKUaaDGwZuGqrvuPFJ27JIz92Qne", "KnYwAfhAROkyob2AKid3zbo7sEd45cwQGAgNUK1nxS4n2AQqlUZrC3k1iYBLwxWORCw9CLjFxRyolnHM2cLnr12n444FypngQMsIe", 101, 10001);
			fail("One of three values invalid");
		}
		catch(NewsagentException e)
		{
			assertEquals("One of three values invalid", e.getMessage());
		}
	}
	
	//viewCustomer JUnits
	
	//Test #: 1
	//Objective: Invalid result set(<1)
	//Input(s): 0
	//Expected Output(s): Exception thrown with "Invalid Result Set" message
	
	//Test #: 2
	//Objective: Valid result set(>0)
	//Input(s): 3
	//Expected Output(s): Exception thrown with "Invalid Result Set" message
	/*
	public void testviewAllCus002()
	{
		Customer testObj = new Customer();
		
		try
		{
			assertEquals(1, testObj.createCustomer("TCvJbeJZxSRBm2i4Emiib82BKUaaDGwZuGqrvuPFJ27JIz92Qn", "KnYwAfhAROkyob2AKid3zbo7sEd45cwQGAgNUK1nxS4n2AQqlUZrC3k1iYBLwxWORCw9CLjFxRyolnHM2cLnr12n444FypngQMsI", 100, 10000)); 
		}
		catch(NewsagentException e)
		{
			fail("No Exception Expected");
		}
	}
	*/
	
}


//Test #:
//Objective:
//Input(s):
//Expected Output(s):
