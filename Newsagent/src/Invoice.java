import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Invoice
{
	
	protected String inv_date, due_date;
	protected double total;
	protected boolean is_paid;
	protected int cus_id;
	protected ResultSet rs = null;
	protected Statement stmt = null;
	
	public Invoice() {
		
	}
	
	public Invoice(String inv_date, String due_date, double total, boolean is_paid, int cus_id) throws InvoiceExceptionHandler
	{
		createInvoice(inv_date, due_date, total, is_paid, cus_id);
	}
	
	public boolean createInvoice(String inv_date, String due_date, double total, boolean is_paid, int cus_id) throws InvoiceExceptionHandler
	{
		if(inv_date.length() != 10 || due_date.length() != 10
				|| total < 0.00 || total > 99999.99
				|| cus_id < 1 || cus_id > Integer.MAX_VALUE -1)
		{
			return false;
			//throw new InvoiceExceptionHandler("One of 4 values is invalid");
		}
		else
		{
			this.inv_date = inv_date;
			this.due_date = due_date;
			this.total = total;
			this.is_paid = is_paid;
			this.cus_id = cus_id;
			return true;
		}
	}
	
	public boolean insertInvoice(Invoice invObj) throws InvoiceExceptionHandler
	{
		try {
			String str = "INSERT INTO Invoice VALUES (null,?,?,?,false,?)";
			Database db = new Database();
			Connection conn = db.getConn();
			PreparedStatement pstmt = conn.prepareStatement(str);
			pstmt.setString(1, invObj.getInvDate());
			pstmt.setString(2, invObj.getDueDate());
			pstmt.setDouble(3, invObj.getTotal());
			pstmt.setInt(4, invObj.getCusId());

			pstmt.executeUpdate();
			return true;
		}
		catch (Exception e) {
			return false;
			//throw new InvoiceExceptionHandler("Something failed in insertInvoice");
		}
	}
	
	public boolean viewAllInvoices()
	{
		String str = "SELECT inv.inv_id, cus.cus_id, cus.name, inv.total, inv.is_paid" + 
				" FROM invoice as inv, customer as cus" + 
				" WHERE cus.cus_id = inv.cus_id" + 
				" ORDER BY inv.inv_id;";
		
        try
        {
        	Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
    		System.out.println();
    		System.out.printf("%s%10s%35s%10s%15s\n", "Inv ID", "Cus ID", "Name", "Total", "Is Paid?");
    		
            rs = stmt.executeQuery(str);
            while (rs.next())
            {
                int inv_id = rs.getInt(1);
                int cus_id = rs.getInt(2);
                String name = rs.getString(3);
                double total = rs.getDouble(4);
                boolean is_paid = rs.getBoolean(5);
                System.out.printf("%-10d%d%40s%10.2f       %b\n", inv_id, cus_id, name, total, is_paid);
            }
            
        }
        catch (SQLException sqle)
        {
        	return false;
			//throw new InvoiceExceptionHandler("Something failed in insertInvoice");
        	
            /*
        	System.out.println("Error: failed to display all invoices.");
            System.out.println(sqle.getMessage());
            System.out.println(str);
            */
        }
        System.out.println();
        return true;
	}
	
	public boolean viewInvoice(int inv_id)
	{
		String strSelect = "SELECT count(*) AS confirm FROM invoice"
						+ " WHERE inv_id = ?;";
    	try
    	{
    		Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
        	PreparedStatement ps = conn.prepareStatement(strSelect);
    		ps.setInt(1, inv_id);

    		//Checks if inv_id exists.
            rs = ps.executeQuery();
            while (rs.next())
            {
                int exists = rs.getInt("confirm");
                
                //Tells user to try again if inv_id doesn't exist.
                if (exists == 0)
                {
                	System.out.println("The invoice with that ID doesn't exist. Please try again.");
                	System.out.println();
                }
                else
                {
                	//Queries for all relevant info.
                	String strShowInvoice = "SELECT inv.inv_id, cus.cus_id, cus.name, cus.address, inv.total, inv.inv_date, inv.due_date, inv.is_paid" + 
            				" FROM invoice as inv, customer as cus " + 
            				" WHERE cus.cus_id = inv.cus_id AND inv.inv_id = ?" + 
            				" ORDER BY inv.inv_id;";
                	
            		//Shows valid invoice
            		try
                    {
            			ps = conn.prepareStatement(strShowInvoice);
                		ps.setInt(1, inv_id);
                		
                		System.out.println();
                		
                        rs = ps.executeQuery();
                        while (rs.next())
                        {
                            String name = rs.getString("name");
                            String address = rs.getString("address");
                            double total = rs.getDouble("total");
                            String inv_date = rs.getString("inv_date");
                            String due_date = rs.getString("due_date");
                            boolean is_paid = rs.getBoolean("is_paid");
                            
                            System.out.println("INVOICE");
                            System.out.println("Name: " +name);
                            System.out.println(address);
                            System.out.println(inv_date);
                            System.out.println(due_date);
                            System.out.println("Invoice No. " + inv_id);
                            System.out.println();
                            System.out.println("DESCRIPTION");
                            //TODO sub type (mag name and "weekly"/"monthly"), unit price
                            System.out.println("THIS IS A PLACEHOLDER MAGAZINE NAME");
                            System.out.println("Unit Price: "+ 200);
                            System.out.println("TOTAL: "+total);
                            if (is_paid == true) {
                            	System.out.println("Paid: Yes");
                            } else {
                            	System.out.println("Paid: No");
                            }
                        }
                    }
                    catch (SQLException sqle)
                    {
                        System.out.println("Error: failed to show the invoice.");
                        System.out.println(sqle.getMessage());
                        System.out.println(strShowInvoice);
                    }
                }
            }
        }
        catch (SQLException sqle)
        {
        	return false;
			//throw new InvoiceExceptionHandler("Something failed in insertInvoice");
        	
        	/*
            System.out.println("Error: failed to search for invoice ID.");
            System.out.println(sqle.getMessage());
            System.out.println(strSelect);
            */
        }
    	return true;
	}
	
	public boolean deleteInvoice(int inv_id) throws InvoiceExceptionHandler
	{
		String strSelect = "SELECT count(*) AS confirm FROM invoice"
						+ " WHERE inv_id = ?;";
    	try
    	{
    		Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
        	PreparedStatement ps = conn.prepareStatement(strSelect);
    		ps.setInt(1, inv_id);

    		//Checks if inv_id exists.
            rs = ps.executeQuery();
            while (rs.next())
            {
                int exists = rs.getInt("confirm");
                
                //Tells user to try again if inv_id doesn't exist.
                if (exists == 0) {
                	System.out.println("The invoice with that ID doesn't exist. Please try again.");
                	System.out.println();
                	return false;
                }
                else
                {
              		String strDelete = "DELETE FROM invoice WHERE inv_id = ?;";
              		
              		try
              		{
              			ps = conn.prepareStatement(strDelete);
              			ps.setInt(1, inv_id);
              			ps.executeUpdate();
              			
              			System.out.println("The invoice was deleted successfully.\n");
              		}
              		catch (SQLException sqle)
            		{
            			System.out.println("Error: failed to delete invoice.");
            			System.out.println(sqle.getMessage());
            			System.out.println(strDelete);
            			return false;
            		}
                }
            }
        }
        catch (SQLException sqle)
        {
			//throw new InvoiceExceptionHandler("Something failed in insertInvoice");
        	
        	/*
            System.out.println("Error: failed to search for invoice ID.");
            System.out.println(sqle.getMessage());
            System.out.println(strSelect);
            */
        }
    	return true;
	}
	
	public void setInvDate(String s)
	{
		inv_date = s;
	}
	
	public void setDueDate(String s)
	{
		due_date = s;
	}
	
	public void setTotal(double d)
	{
		total = d;
	}
	
	public void setIsPaid(boolean b)
	{
		is_paid = b;
	}
	
	public void setCusId(int i)
	{
		cus_id = i;
	}

	public String getInvDate()
	{
		return inv_date;
	}
	
	public String getDueDate()
	{
		return due_date;
	}
	
	public double getTotal()
	{
		return total;
	}
	
	public boolean getIsPaid()
	{
		return is_paid;
	}
	
	public int getCusId()
	{
		return cus_id;
	}
}
