import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database
{	
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
		
	
	public Database() 
	{
		getConn();
	}

	public Connection getConn() 
	{
			try 
			{
				Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
				String url="jdbc:mysql://localhost:3306/newsagent?useTimezone=true&serverTimezone=UTC";
				con = DriverManager.getConnection(url, "root", "admin");
				return con;
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return null;
	}
	
	public void cleanup_resources()
	{
		try
		{
			con.close();
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to close the database");
		}
	}
}