import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Customer 
{	
	private String name, address;
	private int age, dis, cusID;
	boolean isBan;
	boolean result, result2;
	
	Database db = new Database();
	Connection con = db.getConn();
	
	static Statement stmt = null;
	static ResultSet rs = null;
	
	Scanner in = new Scanner(System.in);
	
	public Customer()
	{
	}
	
	public Customer(int cusID, String name, String address, int age, int dis, boolean isBan) throws NewsagentException
	{
		this.cusID = cusID;
		this.name = name;
		this.address = address;
		this.age = age;
		this.dis = dis;
		this.isBan = isBan;
	}
	
	public Customer(String name, String address, int age, int dis) throws NewsagentException
	{
		createCustomer(name, address, age, dis);
	}
	
	public boolean createCustomer(String name, String address, int age, int dis) throws NewsagentException
	{
		if(name.length() < 2 || name.length() > 50 || address.length() < 2 || address.length() > 100 || age < 18 || age > 100 || dis < 1 || dis > 10000)
		{
			throw new NewsagentException("One of three values invalid");
		}
		else
		{
			this.name = name;
			this.address = address;
			this.age = age;
			this.dis = dis;
			result = true;
			return result;
		}
	} 
	
	public void insertCus(Customer cusObj) throws SQLException
	{
		
			String str = "INSERT INTO Customer VALUES (null,?,?,?,?,false)";
			PreparedStatement pstmt = con.prepareStatement(str);
			pstmt.setString(1, cusObj.getName());
			pstmt.setString(2, cusObj.getAddress());
			pstmt.setInt(3, cusObj.getAge());
			pstmt.setInt(4, cusObj.getDis());

			pstmt.executeUpdate();
	}
	
	public void viewAllCustomers()
	{
		//int res = 0;
		String str = "SELECT cus.cus_id, cus.name, cus.address, cus.age, cus.dis_id, cus.is_banned" + 
				" FROM customer as cus, District" + 
				" WHERE cus.dis_id = District.dis_id" + 
				" ORDER BY cus.cus_id;";
		
        try
        {
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
    		System.out.println();
    		System.out.printf("%-10s%10s%40s%30s%20s%20s\n", "Cus ID", "Name", "Address", "Age", "Dis ID", "Is Banned");
    		System.out.println();
    		
            rs = stmt.executeQuery(str);
            while (rs.next())
            {    	
            	//res++;
                int cus_id = rs.getInt(1);
                String name = rs.getString(2);
                String address = rs.getString(3);
                int age = rs.getInt(4);
                int dis_id = rs.getInt(5);
                boolean is_banned = rs.getBoolean(6);
                System.out.printf("%-16d%-37s%-34s%-17d%-13d%9b\n", cus_id, name, address, age, dis_id, is_banned);
            }
        }
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
        System.out.println();
        /*
        if(res > 0)
        {
        	return res;
        }
        else
        {
        	throw new NewsagentException("Invalid Result Set");
        }
        */
	}
	
	public void viewPartCustomer(int id)
	{
		String strSelect = "SELECT count(*) AS confirm FROM Customer, District"
						+ " WHERE customer.cus_id = District.dis_id and cus_id = ?;";
    	try
    	{
    		Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
        	PreparedStatement ps = conn.prepareStatement(strSelect);
    		ps.setInt(1, id);

            rs = ps.executeQuery();
            while (rs.next())
            {
                int exists = rs.getInt("confirm");

                if (exists == 0)
                {
                	System.out.println("The invoice with that ID doesn't exist. Please try again.");
                	System.out.println();
                }
                else
                {
                	String strShowCustomer = "Select Customer.cus_id, Customer.name, Customer.address, Customer.age, Customer.dis_id, Customer.is_banned" + 
            				" FROM Customer, District" + 
            				" WHERE customer.dis_id = district.dis_id AND customer.cus_id = "+id+";"; 
            		try
                    { 				 		
            			System.out.println();
                		System.out.printf("%-10s%10s%40s%30s%20s%20s\n", "Cus ID", "Name", "Address", "Age", "Dis ID", "Is Banned");
                		System.out.println();
                		
                        rs = stmt.executeQuery(strShowCustomer);
                        while (rs.next())
                        {
                            int cus_id = rs.getInt(1);
                            String name = rs.getString(2);
                            String address = rs.getString(3);
                            int age = rs.getInt(4);
                            int dis_id = rs.getInt(5);
                            boolean is_banned = rs.getBoolean(6);
                            System.out.printf("%-16d%-37s%-34s%-17d%-13d%9b\n", cus_id, name, address, age, dis_id, is_banned);
                        }
                    }
            		catch(SQLException sqle)
            		{
            			System.out.println();
            			sqle.getMessage();
            			System.out.println();
            		}
            		catch(Exception e)
            		{
            			System.out.println();
            			e.printStackTrace();
            			System.out.println();
            		}
                }
            }
            System.out.println();
    	}
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}

	public void setName(String s)
	{
		this.name = s;
	}
	
	public void setAddress(String s)
	{
		this.address = s;
	}
	
	public void setAge(int s)
	{
		this.age = s;
	}
	
	public void setDis(int s)
	{
		this.dis = s;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public int getDis()
	{
		return dis;
	}
	
	public String toString()
	{
		return "Customer ID: "+cusID+"\nName: "+name+"\nAddress: "+address+"\nAge: "+age+"\nDistrict ID: "+dis+"\nBanned: "+isBan;
	}

}
/*
 *         	Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
    		System.out.println();
    		System.out.println("cus_id | Name                   | Address                        | Age | dis_id | is_banned");
    		System.out.println("-------------------------------------------------------------------------------------------");
    		 
            rs = stmt.executeQuery(str);
            while (rs.next())
            {
                int cusID = rs.getInt(1); 
                String name = rs.getString(2); 
                String address = rs.getString(3); 
                int age = rs.getInt(4); 
                int disID = rs.getInt(5); 
                boolean isBan = rs.getBoolean(6);
                System.out.println(cusID + "        " + name +"             " +address+"             " +age+"    " +disID+"        " +isBan);
                System.out.println("-------------------------------------------------------------------------------------------");
            }
*/
