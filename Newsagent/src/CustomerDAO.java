import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomerDAO 
{	
	static Scanner in = new Scanner(System.in);
	static Statement stmt = null;
	static ResultSet rs = null;
	
	public CustomerDAO()
	{
	}
	
	public void viewAllCus() throws NewsagentException
	{
		String str = "Select * from Customer where customer.dis_id = district.dis_id;";
		try
		{
            rs = stmt.executeQuery(str); 
            System.out.println();
            while (rs.next()) 
            { 
                int cusID = rs.getInt(1); 
                String name = rs.getString(2); 
                String address = rs.getString(3); 
                int age = rs.getInt(4); 
                int disID = rs.getInt(5); 
                boolean isBan = rs.getBoolean(6); 
                System.out.printf("%-4d%20s%30s%-4d%-4d%20s\n", cusID, name, address, age, disID, isBan); 
            } 
            System.out.println();
		}
		catch(SQLException sqle)
		{
        	System.out.println();
            System.out.println("Error: failed to display all Customers."); 
            System.out.println(sqle.getMessage());
            System.out.println();
		}
	}
	
	
	public List<Customer> getAllCustomers() 
	{
		List<Customer> cusList = new ArrayList<Customer>();
		try
		{
			ResultSet rs = stmt.executeQuery("Select * from Customer;");
			while(rs.next())
			{
				int cusID = rs.getInt(1);
				String name = rs.getString(2);		
				String address = rs.getString(3);		
				int age = rs.getInt(4);
				int dis = rs.getInt(5);
				boolean isBan = rs.getBoolean(6);
				Customer cus = new Customer(cusID, name, address, age, dis, isBan);
				cusList.add(cus);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
		return cusList;
	}
	

	public Customer viewPartCust(int id)
	{
		try
		{
			System.out.print("Please enter the Customer ID you wish to view: ");
			String customerID = in.next();

			String str = "Select count(*) from product where prod_id = "+customerID+";";
			rs = stmt.executeQuery(str);
			
			if(rs.next()) {
				int res = rs.getInt(1); 
				if(res == 0) {
					System.out.println();
					System.out.println("This Customer doesn't exsist!");
					System.out.println();
				}
				else {
					String str2 = "Select Customer.name, Customer.address, Customer.age, Customer.dis_id, Customer.is_banned "+
					"from Customer, District where Customer.dis_id = District.dis_id and Customer.cus_id = "+customerID+";";
					rs = stmt.executeQuery(str2);
							
					System.out.println("Here is the part details for product ID: "+customerID);
		            System.out.println();
		            while (rs.next())
		            {
		                String name = rs.getString("name");
		                String address = rs.getString("address");
		                int age = rs.getInt("age");
		                int dis = rs.getInt("dis_id");
		                boolean ban = rs.getBoolean("ban");
		                
		                System.out.printf("%20s%-4d%20s%30s%8.2f\n", partID, name, desc, cost);
		            } 
		            System.out.println();
				}
			}
		
		}
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}
}

/*
comments!!

public boolean viewCustomer(String str) throws NewsagentException 
	{
		if(str.length() < 10 || str.length() > 10000)
		{
			throw new NewsagentException("Invalid String");
		}
		else
		{
			result2 = true;
			return result2;
		}
	}
	
	public List<Customer> getAllCustomers() 
	{
		List<Customer> cusList = new ArrayList<Customer>();
		try
		{
			ResultSet rs = stmt.executeQuery("Select * from Customer;");
			while(rs.next())
			{
				int cusID = rs.getInt(1);
				String name = rs.getString(2);		
				String address = rs.getString(3);		
				int age = rs.getInt(4);
				int dis = rs.getInt(5);
				boolean isBan = rs.getBoolean(6);
				Customer cus = new Customer(cusID, name, address, age, dis, isBan);
				cusList.add(cus);
			}
		}
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
		return cusList;
	}
	

	public void viewPartCust(int id)
	{
		try
		{
			String str = "Select count(*) from Customer where Customer.dis_id = District.dis_id and cus_id = "+id+";";
			rs = stmt.executeQuery(str);
			
			if(rs.next()) {
				int res = rs.getInt(1); 
				if(res == 0) {
					System.out.println();
					System.out.println("This Customer doesn't exsist!");
					System.out.println();
				}
				else {
					String str2 = "Select Customer.name, Customer.address, Customer.age, Customer.dis_id, Customer.is_banned "+
					"from Customer, District where Customer.dis_id = District.dis_id and Customer.cus_id = "+id+";";
					rs = stmt.executeQuery(str2);
							
					System.out.println("Here is the part details for product ID: "+id);
		            System.out.println();
		            while (rs.next())
		            {
		                String name = rs.getString("name");
		                String address = rs.getString("address");
		                int age = rs.getInt("age");
		                int dis = rs.getInt("dis_id");
		                boolean ban = rs.getBoolean("ban");
		                
		                System.out.printf("%-4d%20s%30s%-4d%-4d%20s\n", name, address, age, dis, ban); 
		            } 
		            System.out.println();
				}
			}
		
		}
		catch(SQLException sqle)
		{
			System.out.println();
			sqle.getMessage();
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}
	

public static void viewAllCus() throws NewsagentException
{
	String str = "Select * from Customer where customer.dis_id = district.dis_id;";
	try
	{
        rs = stmt.executeQuery(str); 
        System.out.println();
        while (rs.next()) 
        { 
            int cusID = rs.getInt(1); 
            String name = rs.getString(2); 
            String address = rs.getString(3); 
            int age = rs.getInt(4); 
            int disID = rs.getInt(5); 
            boolean isBan = rs.getBoolean(6); 
            System.out.printf("%-4d%20s%30s%-4d%-4d%20s\n", cusID, name, address, age, disID, isBan); 
        } 
        System.out.println();
	}
	catch(SQLException sqle)
	{
    	System.out.println();
        System.out.println("Error: failed to display all Customers."); 
        System.out.println(sqle.getMessage());
        System.out.println();
	}
	catch(Exception e)
	{
		System.out.println();
		e.printStackTrace();
		System.out.println();
	}
}

*/
