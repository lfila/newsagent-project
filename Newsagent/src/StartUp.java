import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class StartUp
{			
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) 
	{
		int menuChoice = 0;
		final int STOP_APP = 99; 
		Database clean = new Database();

		
		while (menuChoice != STOP_APP)
		{
			displayMainMenu(); 
			if (in.hasNextInt())
			{
				menuChoice = in.nextInt();
								
				switch (menuChoice)
				{
				case 1: 
					in.nextLine();
					createCusInput();
					break;
					
				case 2:
					in.nextLine();
					viewAllCus();
					break;
					
				case 3:
					in.nextLine();
					viewPartCus();
					break;
					
				case 4:
					in.nextLine();
					createInv();	
					break;
					
				case 5:
					in.nextLine();
					viewInv();
					break;
					
				case 6:
					in.nextLine();
					deleteInv();
					break;
					
				case 7: 
					in.nextLine();
					createDisInput();
					break;
					
				case 8:
					in.nextLine();
					viewOneDis();
					break;
					
				case 9: 
					in.nextLine();
					viewDis();
					break;
					
				case 99:
					System.out.println("Program is closing...");
					clean.cleanup_resources(); 
					break;
				default:
					System.out.println("You entered an invalid choice, please try again...");	
				}
			}
			else
			{
				in.nextLine();
				System.out.println("You entered an invalid choice, please try again...");
			}
		}	
	}
	
	public static void createCusInput() 
	{
		try 
		{			
			System.out.print("Please enter the customers full name: ");
			String cusName = in.nextLine();
			System.out.print("Please enter the customers address\nLine 1: ");
			String add1 = in.nextLine();
			System.out.print("Line 2: ");
			String add2 = in.nextLine();
			System.out.print("Plese enter the customers age: ");
			int cusAge = in.nextInt();
			System.out.print("Plese enter the customers district ID: ");
			int cusDis = in.nextInt();
			in.nextLine();
			
			String cusAddress = add1+", "+add2;
			
			Customer cusObj = new Customer(cusName, cusAddress, cusAge, cusDis);

			cusObj.insertCus(cusObj);
			
			System.out.println();
			System.out.println("You have successfully added a new customer!");
			System.out.println();
		}
		catch(SQLException sqle)
		{
			System.out.println();
			System.out.println("Error: failed to create Customer");
			System.out.println(sqle.getMessage());
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}

	public static void viewAllCus()
	{
		Customer cusViewAll = new Customer();
		cusViewAll.viewAllCustomers();
	}

	public static void viewPartCus() 
	{
		Customer cusViewPart = new Customer();
		System.out.print("Please enter the ID of the customer you wish to view: ");
		int id = in.nextInt();
		cusViewPart.viewPartCustomer(id);
	}
	
	public static void createInv() 
	{
		try 
		{			
			System.out.print("Please enter the invoice issue date: ");
			String inv_date = in.nextLine();
			System.out.print("Please enter the invoice due date: ");
			String due_date = in.nextLine();
			System.out.print("Please enter the total: ");
			double total = in.nextDouble();
			System.out.print("Please enter the customer's ID number: ");
			int cusDis = in.nextInt();
			System.out.print("Is the invoice paid? (true/false)");
			boolean is_paid = in.nextBoolean();
			in.nextLine();
			
			Invoice invObj = new Invoice(inv_date, due_date, total, is_paid, cusDis);

			invObj.insertInvoice(invObj);
			
			System.out.println();
			System.out.println("You have successfully added a new invoice!");
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}
	
	public static void viewInv()
	{
		Invoice invObj = new Invoice();
		
		//Prints out all available invoices.
		invObj.viewAllInvoices();
		
		//Gets inv_id from user input.
		System.out.println("Enter the invoice ID you wish to view:");
		int inv_id = in.nextInt();
		
		//Will print an invoice at the end.
		invObj.viewInvoice(inv_id);
	}
	
	public static void deleteInv() {
		try {
			Invoice invObj = new Invoice();
			
			//Gets inv_id from user input.
			System.out.println("Enter the invoice ID you wish to delete:");
			int inv_id = in.nextInt();
			
			//Will delete invoice if it exists.
			invObj.deleteInvoice(inv_id);
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}
	
	public static void createDisInput() 
	{
		try 
		{			
			System.out.print("Please enter the District Description: ");
			String dis_description = in.nextLine();
			
			District disObj = new District(dis_description);

			disObj.insertDistrict(disObj);
			
			System.out.println();
			System.out.println("You have successfully added a new district!");
			System.out.println();
		}
		catch(SQLException sqle)
		{
			System.out.println();
			System.out.println("Error: failed to create district");
			System.out.println(sqle.getMessage());
			System.out.println();
		}
		catch(Exception e)
		{
			System.out.println();
			e.printStackTrace();
			System.out.println();
		}
	}
	
	public static void viewOneDis()
	{
		District disObj = new District();
		
		//Prints out all available districts.
		disObj.viewAllDistricts();
		
		//Gets dis_id from user input.
		System.out.println("Enter the district ID you wish to view:");
		int dis_id = in.nextInt();
		
		//Will print an district at the end.
		disObj.viewOneDis(dis_id);
	}
	
	public static void viewDis()
	{
		District disObj = new District();
		
		//Prints out all available districts.
		disObj.viewAllDistricts();
	}
	 
	
	public static void displayMainMenu()
	{
		System.out.println("Main Menu\n");
		System.out.println("1: Create Customer");
		System.out.println("2: View All Customers");
		System.out.println("3: View Particular Customer");
		System.out.println("4: Create Invoice");
		System.out.println("5: View Invoice");
		System.out.println("6: Delete Invoice");
		System.out.println("7: Create District");
		System.out.println("8: View One District");
		System.out.println("9: View All Districts");
		System.out.println("99: Exit application\n");
		System.out.println();
		System.out.print("Enter your choice: ");		
	}
}