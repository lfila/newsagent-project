import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class District
{
	protected String dis_description;
	protected ResultSet rs = null;
	protected Statement stmt = null;
	
	public District()
	{
		
	}
	
	public District(String dis_description) throws DistrictExceptionHandler
	{
		createDistrict(dis_description);
	}

	public boolean createDistrict(String dis_description) throws DistrictExceptionHandler
	{
		if(dis_description.length() < 2 || dis_description.length() > 100 )
		{
			return false;
			//throw new DistrictExceptionHandler("The value is invalid!");
		}
		else
		{
			this.dis_description = dis_description;
			return true;
		}
	}
	
	public void insertDistrict(District disObj) throws SQLException
	{
		String str = "INSERT INTO District VALUES (null,?)";
		Database db = new Database();
		Connection conn = db.getConn();
		PreparedStatement pstmt = conn.prepareStatement(str);
		pstmt.setString(1, disObj.getdis_description());
		
		pstmt.executeUpdate();
	}
	
	public void viewAllDistricts()
	{
		String str = "SELECT * " +
		"FROM District " +
		"ORDER BY dis_id"; 
	
	
	try 
	{
		Database db = new Database();
		Connection conn = db.getConn();
		stmt = conn.createStatement();
		
		System.out.println();
		System.out.println("dis_id | dis_description");
		
		//System.out.printf("%s%10s%15s%20s%10s%15s\n", "dis_desription", "dis_id");
		
		rs = stmt.executeQuery(str);
		while(rs.next())
		{
			int dis_id = rs.getInt(1);
			String dis_description = rs.getString(2);
			System.out.println(dis_id + "        " + dis_description);
			//System.out.printf("%- 10d%d%20s%20s%10.2f%b\n", "dis_description", "dis_id");
		}
	}
	catch(SQLException sqle)
	{
		System.out.println("Error: failed to display all Districts");
		System.out.println(sqle.getMessage());
		System.out.println(str);
	}
	System.out.println();
	}
	
	public void viewOneDis(int dis_id)
	{
		String strSelect = "SELECT count(*) as confirm " + 
				"FROM district " + 
				"WHERE dis_id = ?;";
    	try
    	{
    		Database db = new Database();
    		Connection conn = db.getConn();
    		stmt = conn.createStatement();
    		
        	PreparedStatement ps = conn.prepareStatement(strSelect);
    		ps.setInt(1, dis_id);

    		//Checks if dis_id exists.
            rs = ps.executeQuery();
            while (rs.next())
            {
                int exists = rs.getInt("confirm");
                
                //Tells user to try again if dis_id doesn't exist.
                if (exists == 0)
                {
                	System.out.println("The district with that ID doesn't exist. Please try again.");
                	System.out.println();
                }
                else
                {
                	//Queries for all relevant info.
                	String strShowDistrict = "SELECT cus.cus_id, cus.name, cus.address, dis.dis_description, cus.age, cus.is_banned " + 
            				" FROM district as dis, customer as cus " + 
            				" WHERE cus.dis_id = dis.dis_id AND dis.dis_id = ? " + 
            				" ORDER BY dis.dis_id;";
                	
            		//Shows valid district
            		try
                    {
            			ps = conn.prepareStatement(strShowDistrict);
                		ps.setInt(1, dis_id);
                		
                		System.out.println();
                		
                        rs = ps.executeQuery();
                        System.out.println("Customers Living at district " + dis_id);
                        System.out.println("Customer ID      Name          Address        District     Age    Banned");
                        while (rs.next())
                        {
                            int cus_id = rs.getInt("cus_id");
                            String name = rs.getString("name");
                            String address = rs.getString("address");
                            String dis_description = rs.getString("dis_description");
                            int age = rs.getInt("age");
                            boolean is_banned = rs.getBoolean("is_banned");
                            
                            System.out.print(cus_id + "            " + name + "   " + "   " + address + "   " + dis_description + "   " + age + "     ");
                            if (is_banned == true) {
                            	System.out.println("Banned: Yes");
                            } else {
                            	System.out.println("Banned: No");
                            }
                        }
                    }
                    catch (SQLException sqle)
                    {
                        System.out.println("Error: failed to show the district.");
                        System.out.println(sqle.getMessage());
                        System.out.println(strShowDistrict);
                    }
                }
            }
        }
        catch (SQLException sqle)
        {
            System.out.println("Error: failed to search for district ID.");
            System.out.println(sqle.getMessage());
            System.out.println(strSelect);
        }
	}

	public void viewDistrict(int dis_id)
	{
		String strSelect = "SELECT count(*) AS confirm FROM district "
				+ "WHERE dis_id = ?;";
		try
		{
			Database db = new Database();
			Connection conn = db.getConn();
			stmt = conn.createStatement();
			
			PreparedStatement ps = conn.prepareStatement(strSelect);
			ps.setInt(1, dis_id);
			
			//Check if dis_id exists
			rs = ps.executeQuery();
			while (rs.next())
			{
				int exists = rs.getInt("confirm");
				
				//Tells the user to try again if dis_id does not exist
				if (exists == 0)
				{
					System.out.println("The district with that ID does not exist. Please Try again.");
					System.out.println();
				}
				else
				{
					//Queries for all relevant info.
					String strShowDistrict = "SELECT * " + 
							"FROM District " +
							"ORDER BY dis_id;";
					
					// Shows valid districts
					try
					{
						ps = conn.prepareStatement(strShowDistrict);
						ps.setInt(1, dis_id);
						
						System.out.println();
						
						rs = ps.executeQuery();
						while (rs.next())
						{
							String dis_description = rs.getString("dis_description");
							
							System.out.println("District");
							System.out.println("Description");
						}
					}
					catch(SQLException sqle)
					{
						System.out.println("Error: failed to show District.");
						System.out.println(sqle.getMessage());
						System.out.println(strShowDistrict);
					}
				}
			}
		}
		catch (SQLException sqle)
		{
			System.out.println("Error: failed to search for District ID");
			System.out.println(sqle.getMessage());
			System.out.println(strSelect);
		}
	}

		public boolean deleteDistrict(String dis_description, int dis_id) throws DistrictExceptionHandler
		{
			throw new RuntimeException("RuntimeException: Product code not yet written.");
		}
		
		public boolean modifyDistrict(String dis_desription, int dis_id) throws DistrictExceptionHandler
		{
			throw new RuntimeException("RuntimeException: Product code not yet written.");
		}
	
	public void setdis_description(String s)
	{
		dis_description = s;
	}
	
	public String getdis_description()
	{
		return dis_description;
	}
}
//Milestone2: Print all customers at the specific district id (Select * from Customer WHERE cus_id = ?) PreparedStatement
//Milestone3:  
// 0 --- 0     1 --- 24    25 --- max
// 0 --- 1     2 --- 100   101 --- 150