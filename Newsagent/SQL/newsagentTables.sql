DROP DATABASE IF EXISTS newsagent;
CREATE DATABASE IF NOT EXISTS newsagent;
USE newsagent;



SELECT 'CREATING District TABLE' as 'INFO';

DROP TABLE IF EXISTS District;

CREATE TABLE District (
	dis_id INTEGER AUTO_INCREMENT ,
	district_description VARCHAR(100) NOT NULL,
	PRIMARY KEY(dis_id));
	
SELECT 'INSERTING DATA INTO TABLE District' as 'INFO';

INSERT INTO District VALUES ( null, 'Willow Park');
INSERT INTO District VALUES ( null, 'St Marys Church');
INSERT INTO District VALUES ( null, 'AIT');
INSERT INTO District VALUES ( null, 'Newtown Terrace');
INSERT INTO District VALUES ( null, 'Grace Park');
INSERT INTO District VALUES ( null, 'Fair Green');
INSERT INTO District VALUES ( null, 'Coosen Point');
INSERT INTO District VALUES ( null, 'Assumption Road');
INSERT INTO District VALUES ( null, 'Auburn');
INSERT INTO District VALUES ( null, 'Beech Park West');
INSERT INTO District VALUES ( null, 'Churchfields');
INSERT INTO District VALUES ( null, 'Oakdale');
INSERT INTO District VALUES ( null, 'The Batteries');
INSERT INTO District VALUES ( null, 'Dun Ard');
INSERT INTO District VALUES ( null, 'Magazine');
INSERT INTO District VALUES ( null, 'Connaught Gardens');
INSERT INTO District VALUES ( null, 'Canal Banks');
INSERT INTO District VALUES ( null, 'Pact');
INSERT INTO District VALUES ( null, 'Ardri');
INSERT INTO District VALUES ( null, 'Brideswell Street');
INSERT INTO District VALUES ( null, 'Retreet Ridge');
INSERT INTO District VALUES ( null, 'Brawney Square');
INSERT INTO District VALUES ( null, 'Garanafailagh');
INSERT INTO District VALUES ( null, 'Coosen Point Road');


SELECT 'CHECKING TO SEE IF DATA INSERTED TO District CORRECTLY' as 'INFO';

SELECT 'CREATING Customer TABLE' as 'INFO';

DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
	cus_id INTEGER AUTO_INCREMENT ,
	name VARCHAR(50),
	address VARCHAR(100) NOT NULL,
    age INTEGER NOT NULL,
	dis_id INTEGER NOT NULL,
	is_banned BOOLEAN NOT NULL,
	PRIMARY KEY(cus_id),
	FOREIGN KEY(dis_id) REFERENCES District(dis_id));
	
SELECT 'INSERTING DATA INTO TABLE Customer' as 'INFO';

INSERT INTO Customer VALUES ( null, 'David Duggan', 'Willow Park, Athlone', 20, 1, false);
INSERT INTO Customer VALUES ( null, 'Lukasz Fila', 'AIT, Athlone', 19, 3, true);
INSERT INTO Customer VALUES ( null, 'Tiger Woods', 'Beech Park West, Athlone', 44, 10, false);
INSERT INTO Customer VALUES ( null, 'Bob Ross', 'Oakdale, Athlone', 55, 12, false);
INSERT INTO Customer VALUES ( null, 'John McGibby', 'St Marys Church, Athlone', 77, 2, false);


SELECT 'CHECKING TO SEE IF DATA INSERTED TO Customer CORRECTLY' as 'INFO';

SELECT * FROM Customer;


SELECT 'CREATING Invoice TABLE' as 'INFO';

DROP TABLE IF EXISTS invoice;

CREATE TABLE invoice (
	inv_id INTEGER AUTO_INCREMENT,
	inv_date VARCHAR(10) NOT NULL,
	due_date VARCHAR(10) NOT NULL,
	total DECIMAL(7,2) NOT NULL,
	is_paid BOOLEAN NOT NULL,
	cus_id INTEGER,
	PRIMARY KEY(inv_id),
	FOREIGN KEY(cus_id) REFERENCES Customer(cus_id));

SELECT 'INSERTING DATA INTO TABLE invoice' as 'INFO';

INSERT INTO invoice VALUES (null, '01/02/2020', '07/02/2020', 120.00, false, 1);
INSERT INTO invoice VALUES (null, '01/02/2020', '14/02/2020', 220.00, true, 2);
INSERT INTO invoice VALUES (null, '01/02/2020', '14/02/2020', 300.00, false, 3);
INSERT INTO invoice VALUES (null, '01/02/2020', '14/02/2020', 300.00, false, 2);
INSERT INTO invoice VALUES (null, '01/02/2020', '14/02/2020', 300.00, false, 5);
INSERT INTO invoice VALUES (null, '01/02/2020', '14/02/2020', 300.00, false, 4);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO Invoice CORRECTLY' as 'INFO';

SELECT * FROM invoice;

SELECT 'CREATING delivery TABLE' as 'INFO';

DROP TABLE IF EXISTS Delivery;

CREATE TABLE Delivery (
	del_id INTEGER AUTO_INCREMENT ,
	cus_id INTEGER,
	inv_id INTEGER,
	date VARCHAR(10) NOT NULL,
	PRIMARY KEY(del_id),
	FOREIGN KEY(cus_id) REFERENCES Customer(cus_id),
	FOREIGN KEY(inv_id) REFERENCES Invoice(inv_id));

SELECT 'INSERTING DATA INTO TABLE Delivery' as 'INFO';

INSERT INTO Delivery VALUES ( null, 2, 1, 1);
INSERT INTO Delivery VALUES ( null, 2, 2, 2);
INSERT INTO Delivery VALUES ( null, 2, 2, 4);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO Delivery CORRECTLY' as 'INFO';

SELECT * FROM Delivery;